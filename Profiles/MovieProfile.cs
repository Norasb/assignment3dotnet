﻿using AutoMapper;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTO;
using MovieCharactersAPI.Models.DTO.Movies;

namespace MovieCharactersAPI.Profiles 
{
    /// <summary>
    /// Mappings for Movie entity to various Dtos
    /// </summary>

    public class MovieProfile : Profile 
    {
        public MovieProfile() 
        {
            CreateMap<MoviePostDto, Movie>();

            CreateMap<Movie, MovieDto>()
                .ForMember(dto => dto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(c => c.FullName).ToList()))
                .ForMember(dto => dto.Franchise, opt => opt
                .MapFrom(m => m.Franchise.FranchiseName));

            CreateMap<MoviePutDto, Movie>();

        }
    }
}
