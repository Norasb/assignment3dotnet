﻿using AutoMapper;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTO.Franchise;

namespace MovieCharactersAPI.Profiles
{
    /// <summary>
    /// Mappings for Franchise entity to various Dtos
    /// </summary>
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile() 
        {
            CreateMap<Franchise, FranchiseDTO>()
                .ForMember(dto => dto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(m => m.MovieTitle).ToList()));
            CreateMap<FranchisePostDTO, Franchise>();
            CreateMap<FranchisePutDTO, Franchise>();
        }
    }
}
