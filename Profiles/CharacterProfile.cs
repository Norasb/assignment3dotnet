﻿using AutoMapper;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTO.Characters;

namespace MovieCharactersAPI.Profiles
{
    /// <summary>
    /// Mappings for Character entity to various Dtos
    /// </summary>
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterDTO>()
                .ForMember(dto => dto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(m => m.MovieId).ToList()));
            CreateMap<CharacterPostDTO, Character>();
            CreateMap<CharacterPutDTO, Character>();
        }
    }
}
