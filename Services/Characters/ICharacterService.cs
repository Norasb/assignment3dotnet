﻿using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Services.Characters
{
    public interface ICharacterService: ICrudService<Character, int>
    {
    }
}
