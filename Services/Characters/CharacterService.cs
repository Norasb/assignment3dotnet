﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Services.Characters
{
    /// <summary>
    /// This class has the logic surrounding a character
    /// </summary>
    public class CharacterService : ICharacterService
    {
        private readonly MovieCharacterContext _context;

        public CharacterService(MovieCharacterContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Character obj)
        {
            await _context.AddAsync(obj);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return;
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
            return;
        }

        public async Task<ICollection<Character>> GetAllAsync()
        {
            return await _context.Characters.ToListAsync();
        }

        public async Task<Character> GetByIdAsync(int id)
        {
           return await _context.Characters.FindAsync(id);

        }

        public async Task UpdateAsync(Character obj)
        {
           _context.Entry(obj).State = EntityState.Modified;
           await _context.SaveChangesAsync();
           return;
        }
        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.CharacterId == id);
        }
    }
}
