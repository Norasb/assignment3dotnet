﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Services.Movies
{
    public interface IMovieService : ICrudService<Movie, int>
    {
        /// <summary>
        /// Gets the characters in a movie
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns>Returns a collection of characters</returns>
        Task<ICollection<Character>> GetCharactersInMovieAsync(int movieId);
        /// <summary>
        /// Adds existing chosen characters to a movie
        /// </summary>
        /// <param name="characterIds"></param>
        /// <param name="movieId"></param>
        Task UpdateCharactersInMovieAsync(int[] characterIds, int movieId);
    }
}
