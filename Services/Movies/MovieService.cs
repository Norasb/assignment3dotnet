﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using System.Text.Json.Serialization;

namespace MovieCharactersAPI.Services.Movies
{
    /// <summary>
    /// This class has the logic surrounding a Movie
    /// </summary>
    public class MovieService : IMovieService
    {
        private readonly MovieCharacterContext _context;
        private readonly ILogger<MovieService> _logger;

        public MovieService(MovieCharacterContext context, ILogger<MovieService> logger)
        {
            _context = context;
            _logger = logger;
        }


        public async Task AddAsync(Movie obj) 
        {
            await _context.AddAsync(obj);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<Movie>> GetAllAsync()
        {
            return await _context.Movies
                .Include(prop => prop.Characters)
                .Include(prop => prop.Franchise)
                .ToListAsync();
        }

        public async Task<Movie> GetByIdAsync(int id) 
        {
            if (!await MovieExistsAsync(id)) 
            {
                _logger.LogError("Movie with id " + id + " not found!");
                throw new EntryPointNotFoundException();
            }
            return await _context.Movies
                .Where(prop => prop.MovieId == id)
                .Include(prop => prop.Characters)
                .Include(prop => prop.Franchise)
                .FirstAsync();
        }



        public async Task<ICollection<Character>> GetCharactersInMovieAsync(int movieId) 
        {
            if (!await MovieExistsAsync(movieId)) 
            {
                _logger.LogError("Movie with id " + movieId + " not found!");
                throw new EntryPointNotFoundException();
            }
            return await _context.Movies
                .Where(m => m.MovieId == movieId)
                .Include(m => m.Characters)
                .Select(m => m.Characters)
                .FirstAsync();
        }


        public async Task UpdateAsync(Movie obj)
        {
            if (!await MovieExistsAsync(obj.MovieId)) 
            {
                _logger.LogError("Movie with id " + obj.MovieId + " not found!");
                throw new EntryPointNotFoundException();
            }
            _context.Entry(obj).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateCharactersInMovieAsync(int[] charactersIds, int movieId) 
        {
            if (!await MovieExistsAsync(movieId)) 
            {
                _logger.LogError("Movie with id " + movieId + " not found!");
                throw new EntryPointNotFoundException();
            }
            //Convert the int[] to List<Character>
            List<Character> characters = charactersIds
                .ToList()
                .Select(cid => _context.Characters
                .Where(c => c.CharacterId == cid).First())
                .ToList();
            //Get movie for id
            Movie movie = await _context.Movies
                .Where(m => m.MovieId == movieId)
                .FirstAsync();
            //Set the movies characters
            movie.Characters = characters;
            _context.Entry(movie).State = EntityState.Modified;
            //Save all the changes
            await _context.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id) 
        {
            var movie = await _context.Movies.FindAsync(id);

            if (movie == null) 
            {
                _logger.LogError("Movie with id " + id + " not found!");
                throw new EntryPointNotFoundException();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }


        private async Task<bool> MovieExistsAsync(int id) 
        {
            return await _context.Movies.AnyAsync(e => e.MovieId == id);
        }

        
    }
}
