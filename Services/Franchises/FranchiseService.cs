﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Services.Franchises 
    {
    /// <summary>
    /// This class has the logic surrounding a Franchise
    /// </summary>
    public class FranchiseService : IFranchiseService 
    {

        private readonly MovieCharacterContext _context;
        private readonly ILogger<FranchiseService> _logger;

        public FranchiseService(MovieCharacterContext context, ILogger<FranchiseService> logger) 
        {
            _context = context;
            _logger = logger;
        }
        public async Task AddAsync(Franchise obj) 
        {
            await _context.AddAsync(obj);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<Franchise>> GetAllAsync() 
        {
            return await _context.Franchises
                .Include(prop => prop.Movies)
                .ToListAsync();
        }

        public async Task<Franchise> GetByIdAsync(int id) 
        {
            if (!await FranchiseExistsAsync(id)) {
                _logger.LogError("Franchise with id " + id + " not found!");
                throw new EntryPointNotFoundException();
            }
            return await _context.Franchises
                .Where(prop => prop.FranchiseId == id)
                .Include(prop => prop.Movies)
                .FirstAsync();
        }

        public async Task<ICollection<Movie>> GetMoviesInFranchiseAsync(int franchiseId) 
        {
            if (!await FranchiseExistsAsync(franchiseId)) 
            {
                _logger.LogError("Franchise with id " + franchiseId + " not found!");
                throw new EntryPointNotFoundException();
            }
            return await _context.Franchises
                .Where(f => f.FranchiseId == franchiseId)
                .Include(m => m.Movies)
                .Select(m => m.Movies)
                .FirstAsync();
        }

        public async Task UpdateAsync(Franchise obj) 
        {
            if (!await FranchiseExistsAsync(obj.FranchiseId)) 
            {
                _logger.LogError("Franchise with id " + obj.FranchiseId + " not found!");
                throw new EntryPointNotFoundException();
            }
            _context.Entry(obj).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task DeleteByIdAsync(int id) 
        {
            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null) 
            {
                _logger.LogError("Franchise with id " + id + " not found!");
                throw new EntryPointNotFoundException();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateMoviesInFranchiseAsync(int[] movieIds, int franchiseId) 
        {
            if (!await FranchiseExistsAsync(franchiseId)) 
            {
                _logger.LogError("Franchise with id " + franchiseId + " not found!");
                throw new EntryPointNotFoundException();
            }
            //Convert the int[] to List<Character>
            List<Movie> movies = movieIds
                .ToList()
                .Select(mid => _context.Movies
                .Where(m => m.MovieId == mid).First())
                .ToList();
            //Get movie for id
            Franchise franchise = await _context.Franchises
                .Where(f => f.FranchiseId == franchiseId)
                .FirstAsync();
            //Set the movies characters
            franchise.Movies = movies;
            _context.Entry(franchise).State = EntityState.Modified;
            //Save all the changes
            await _context.SaveChangesAsync();
        }
        private async Task<bool> FranchiseExistsAsync(int id) 
        {
            return await _context.Franchises.AnyAsync(e => e.FranchiseId == id);
        }
    }
}
