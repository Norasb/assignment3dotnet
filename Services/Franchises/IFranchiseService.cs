﻿using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Services.Franchises 
{
    public interface IFranchiseService: ICrudService<Franchise, int> 
    {
        /// <summary>
        /// Gets all the movies in a specific franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns>A collection of movies</returns>
        Task<ICollection<Movie>> GetMoviesInFranchiseAsync(int franchiseId);
        /// <summary>
        /// Adds existing chosen movies to the franchise
        /// </summary>
        /// <param name="moviesId"></param>
        /// <param name="franchiseId"></param>
        Task UpdateMoviesInFranchiseAsync(int[] moviesId, int franchiseId);
    }
}
