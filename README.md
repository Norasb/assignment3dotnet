# MovieCharacterApi

An Api with movie, characters and franchises. Movies have characters and franchise connected to it, Franchise have movies connected to it. Api endpoint can be seen in swagger when project is run. 

## Usage

Clone project
Change Data source in appsettings to datasource in your SQL server
Use PMC to migrate and update database to create database
Run project - Swagger will open with the endpoints available


## Contributing

Nora Sophie Backe and Lars Joar Bjørkeland
