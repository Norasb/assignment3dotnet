﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models.DTO.Characters
{
    /// <summary>
    /// Dto for character used for creating a new character
    /// </summary>
    public class CharacterPostDTO
    {
        [MaxLength(50)]
        public string FullName { get; set; } = null!;
        [MaxLength(50)]
        public string Alias { get; set; } = null!;
        [MaxLength(50)]
        public string Gender { get; set; } = null!;
        [MaxLength(300)]
        public string CharacterPicture { get; set; } = null!;
    }
}
