﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models.DTO.Characters
{
    /// <summary>
    /// Genereal dto for character
    /// </summary>
    public class CharacterDTO
    {
        public int CharacterId { get; set; }
        public string FullName { get; set; } = null!;
        [MaxLength(50)]
        public string Alias { get; set; } = null!;
        [MaxLength(50)]
        public string Gender { get; set; } = null!;
        [MaxLength(300)]
        public string CharacterPicture { get; set; } = null!;
        public List<int> Movies { get; set; } = null!;

    }
}
