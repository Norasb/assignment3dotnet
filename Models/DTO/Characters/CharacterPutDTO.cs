﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models.DTO.Characters
{
    /// <summary>
    /// Dto for character used when a character is changed
    /// </summary>
    public class CharacterPutDTO
    {
        public int CharacterId { get; set; }
        public string FullName { get; set; } = null!;
        [MaxLength(50)]
        public string Alias { get; set; } = null!;
        [MaxLength(50)]
        public string Gender { get; set; } = null!;
        [MaxLength(300)]
        public string CharacterPicture { get; set; } = null!;
    }
}
