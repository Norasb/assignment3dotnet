﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models.DTO.Movies 
{
    /// <summary>
    /// Dto for movie used when a movie is changed
    /// </summary>
    public class MoviePutDto 
    {
        [MaxLength(50)]
        public string MovieTitle { get; set; } = null!;
        [MaxLength(20)]
        public string Genre { get; set; } = null!;
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; } = null!;
        [MaxLength(300)]
        public string MoviePicture { get; set; } = null!;
        [MaxLength(300)]
        public string Trailer { get; set; } = null!;
    }
}
