﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models.DTO 
{
    /// <summary>
    /// General Dto for Movie - Includes all info but characters and franchise only show the names
    /// </summary>
    public class MovieDto 
    {
        public int MovieId { get; set; }
        [MaxLength(50)]
        public string MovieTitle { get; set; } = null!;
        [MaxLength(20)]
        public string Genre { get; set; } = null!;
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; } = null!;
        [MaxLength(300)]
        public string MoviePicture { get; set; } = null!;
        [MaxLength(300)]
        public string Trailer { get; set; } = null!;
        public List<string> Characters { get; set; } = null!;
        public string Franchise { get; set; }
    }
}
