﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models.DTO.Franchise
{
    /// <summary>
    /// Dto for franchise used when a franchise is changed
    /// </summary>
    public class FranchisePutDTO
    {
        public int FranchiseId { get; set; }
        [MaxLength(50)]
        public string FranchiseName { get; set; } = null!;
        [MaxLength(100)]
        public string Description { get; set; } = null!;
    }
}
