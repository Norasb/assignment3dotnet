﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models.DTO.Franchise
{
    /// <summary>
    /// General dto for franchise that is showing movies with only the movietitle
    /// </summary>
    public class FranchiseDTO
    {
        public int FranchiseId { get; set; }
        [MaxLength(50)]
        public string FranchiseName { get; set; } = null!;
        [MaxLength(100)]
        public string Description { get; set; } = null!;

        public List<string> Movies { get; set; } = null!;
    }
}
