﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models.DTO.Franchise
{
    /// <summary>
    /// Dto for franchise used when a new franchise is created
    /// </summary>
    public class FranchisePostDTO
    {
        public string FranchiseName { get; set; } = null!;
        [MaxLength(100)]
        public string Description { get; set; } = null!;
    }
}
