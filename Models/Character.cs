﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersAPI.Models 
{
    [Table("Character")]
    public class Character 
    {

        public Character()
        {
            Movies = new HashSet<Movie>();
        }
        public int CharacterId { get; set; }
        [MaxLength(50)]
        public string FullName { get; set; } = null!;
        [MaxLength(50)]
        public string Alias { get; set; } = null!;
        [MaxLength(50)]
        public string Gender { get; set; } = null!;
        [MaxLength(300)]
        public string CharacterPicture { get; set; } = null!;

        //Relationships
        public virtual ICollection<Movie> Movies { get; set; } = new HashSet<Movie>();
    }
}
