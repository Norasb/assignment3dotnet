﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersAPI.Models 
{
    [Table("Franchise")]
    public class Franchise 
    {

        public Franchise() 
        {
            Movies = new HashSet<Movie>();
        }
        public int FranchiseId { get; set; }
        [MaxLength(50)]
        public string FranchiseName { get; set; } = null!;
        [MaxLength(100)]
        public string Description { get; set; } = null!;

        public virtual ICollection<Movie> Movies { get; set; } = new HashSet<Movie>(); 
    }

}
