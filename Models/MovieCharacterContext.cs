﻿using Microsoft.EntityFrameworkCore;

namespace MovieCharactersAPI.Models 
{
    public class MovieCharacterContext : DbContext 
    {
        public MovieCharacterContext(DbContextOptions options) : base(options) 
        {
        }

        public DbSet<Character> Characters { get; set; } = null!;
        public DbSet<Movie> Movies { get; set; } = null!;
        public DbSet<Franchise> Franchises { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder) 
        {

            //Movies
            modelBuilder.Entity<Movie>().HasData(
                new Movie() { MovieId = 1, MovieTitle = "Harry Potter and the Goblet of Fire", Genre = "Fantasy", ReleaseYear = 2005, Director = "Mike Newell", MoviePicture = "https://static.wikia.nocookie.net/qghficsimjkaeibhfztnpjrqiezhzuadzsjxwpnxusefbthfes/images/4/4a/35FFD453-DFDD-4C2C-93C2-FDE66CCB14D6.webp/revision/latest?cb=20210812224339", Trailer = "https://www.youtube.com/watch?v=PFWAOnvMd1Q" },
                new Movie() { MovieId = 2, MovieTitle = "Harry Potter and the Half-Blood Prince", Genre = "Fantasy", ReleaseYear = 2009, Director = "David Yates", MoviePicture = "https://static.wikia.nocookie.net/harrypotter/images/4/45/Harry_and_Dumbledore_HBPF_promo.jpg/revision/latest?cb=20141215114252", Trailer = "https://www.youtube.com/watch?v=tAiy66Xrsz4" },
                new Movie() { MovieId = 3, MovieTitle = "Lord of the Rings: Two Towers", Genre = "Fantasy", ReleaseYear = 2002, Director = "Peter Jackson", MoviePicture = "https://upload.wikimedia.org/wikipedia/en/2/2c/Twotowersbox1.jpg", Trailer = "https://www.youtube.com/watch?v=Y4neBR0h39c" }
                );

            //Characters
            modelBuilder.Entity<Character>().HasData(
                new Character() { CharacterId = 1, FullName = "Hermione Granger", Alias = "Hermione", Gender = "Female", CharacterPicture = "https://i0.wp.com/the-art-of-autism.com/wp-content/uploads/2022/12/Hermione-Granger.jpg?fit=450%2C600&ssl=1" },
                new Character() { CharacterId = 2, FullName = "Harry Potter", Alias = "Potter", Gender = "Male", CharacterPicture = "https://bok365.no/wp-content/uploads/2016/04/Potter-e1459547925913.jpg" },
                new Character() { CharacterId = 3, FullName = "Ron Weasley", Alias = "Ron", Gender = "Male", CharacterPicture = "https://upload.wikimedia.org/wikipedia/en/thumb/5/5e/Ron_Weasley_poster.jpg/220px-Ron_Weasley_poster.jpg" },
                new Character() { CharacterId = 4, FullName = "Frodo Lummelun", Alias = "Frodo", Gender = "Male", CharacterPicture = "https://static.wikia.nocookie.net/lotr/images/3/32/Frodo_%28FotR%29.png/revision/latest?cb=20221006065757" },
                new Character() { CharacterId = 5, FullName = "Arwen", Alias = "Arwen", Gender = "Female", CharacterPicture = "https://static.wikia.nocookie.net/lotr/images/e/e9/Arwen_-_Sword.jpg/revision/latest?cb=20210625164216" }
                );

            //Franchises
            modelBuilder.Entity<Franchise>().HasData(
                new Franchise() { FranchiseId = 1, FranchiseName = "Harry Potter", Description = "A movie where the main character never knows what happens" },
                new Franchise() { FranchiseId = 2, FranchiseName = "Lord of the Rings", Description = "Trying to not lose a ring" }
                );

            modelBuilder.Entity<Movie>()
                .HasMany(mov => mov.Characters)
                .WithMany(cha => cha.Movies)
                .UsingEntity<Dictionary<string, object>>(
                "CharacterMovie",
                l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                je => 
                {
                    je.HasKey("MovieId", "CharacterId");
                    je.HasData(
                        new { MovieId = 1, CharacterId = 1 },
                        new { MovieId = 1, CharacterId = 2 },
                        new { MovieId = 1, CharacterId = 3 },
                        new { MovieId = 2, CharacterId = 1 },
                        new { MovieId = 2, CharacterId = 3 },
                        new { MovieId = 3, CharacterId = 4 },
                        new { MovieId = 3, CharacterId = 5 }
                        );
                }
                );
        }
    }
}
