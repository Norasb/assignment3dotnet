﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersAPI.Models 
{
    [Table("Movie")]
    public class Movie 
    {

        public int MovieId { get; set; }
        [MaxLength(50)]
        public string MovieTitle { get; set; } = null!;
        [MaxLength(20)]
        public string Genre { get; set; } = null!;
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; } = null!;
        [MaxLength(300)]
        public string MoviePicture { get; set; } = null!;
        [MaxLength(300)]
        public string Trailer { get; set; } = null!;

        //Relationships
        public virtual Franchise? Franchise { get; set; }
        public virtual ICollection<Character> Characters { get; set; } = new List<Character>();

    }
}
