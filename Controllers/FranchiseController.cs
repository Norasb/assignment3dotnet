﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTO;
using MovieCharactersAPI.Models.DTO.Franchise;
using MovieCharactersAPI.Services.Franchises;
using System.Net;

namespace MovieCharactersAPI.Controllers {
    [Route("franchises")]
    [ApiController]
    public class FranchiseController : ControllerBase {
        private readonly IFranchiseService _franchiseService;
        private readonly IMapper _mapper;

        public FranchiseController(IFranchiseService iFranchiseService, IMapper mapper) {
            _franchiseService = iFranchiseService;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns all franchises from the database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDTO>>> GetAllFranchises() {
            var franchises = await _franchiseService.GetAllAsync();
            return Ok(_mapper.Map<List<FranchiseDTO>>(franchises));
        }

        /// <summary>
        /// Returns a franchise by its id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDTO>> GetFranchiseById(int id) {
            var franchise = await _franchiseService.GetByIdAsync(id);
            try {
                return Ok(_mapper.Map<FranchiseDTO>(franchise));
            } catch (Exception ex) {
                return NotFound(
                    new ProblemDetails() {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }
        /// <summary>
        /// Gets all movies in a franchise by its id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieDto>>> GetMoviesFromFranchiseAsync(int id) {
            try {
                return Ok(
                    _mapper.Map<List<MovieDto>>(
                    await _franchiseService.GetMoviesInFranchiseAsync(id)));
            } catch (Exception ex) {
                // Formatting an error code for the exception messages. 
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails() {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }
        /// <summary>
        /// Post a new franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> PostFranchise(FranchisePostDTO franchise) {
            Franchise f = _mapper.Map<Franchise>(franchise);
            await _franchiseService.AddAsync(f);
            return CreatedAtAction("GetFranchiseById", new { id = f.FranchiseId }, f);
        }
        /// <summary>
        /// Edit an existing franchise by its id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchiseAsync(int id, FranchisePutDTO franchise) {

            /*Franchise putFranchise = new Franchise() {
                FranchiseId = id,
                FranchiseName = franchise.FranchiseName,
                Description = franchise.Description
            };*/

            try {
                await _franchiseService.UpdateAsync(_mapper.Map<Franchise>(franchise));
                return NoContent();
            } catch (Exception ex) {
                return NotFound(
                    new ProblemDetails() {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                );
            }
        }
        /// <summary>
        /// Update the movies in a franchise by its id
        /// </summary>
        /// <param name="movieIds"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateMoviesForFranchiseAsync(int[] movieIds, int id) {
            try {
                await _franchiseService.UpdateMoviesInFranchiseAsync(movieIds, id);
                return NoContent();
            } catch (Exception ex) {
                return NotFound(
                    new ProblemDetails() {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }
        /// <summary>
        /// Delete a franchise by its id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteFranchiseAsync(int id) {
            try {
                await _franchiseService.DeleteByIdAsync(id);
                return NoContent();
            } catch (Exception ex) {
                return NotFound(
                    new ProblemDetails() {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }
    }
}
