﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTO.Characters;
using MovieCharactersAPI.Services.Characters;
using MovieCharactersAPI.Services.Movies;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CharacterController : ControllerBase
    {
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;

        public CharacterController(ICharacterService characterService, IMapper mapper)
        {
            _characterService = characterService;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns all the characters in the database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> GetAllAsync()
        {
            var characters = await _characterService.GetAllAsync();
            return Ok(_mapper.Map<List<CharacterDTO>>(characters));
        }

        /// <summary>
        /// Returns a character from the database based on their id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDTO>> GetByIdAsync(int id)
        {
            var character = await _characterService.GetByIdAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<CharacterDTO>(character));
        }

        /// <summary>
        /// Updates the parameters of a posted character
        /// </summary>
        /// <param name="id"></param>
        /// <param name="character"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterPutDTO character)
        {
            if (id != character.CharacterId)
            {
                return BadRequest();
            }

            await _characterService.UpdateAsync(
                _mapper.Map<Character>(character)
                    );
            return NoContent();

        }

    /// <summary>
    /// Post a character to the database
    /// </summary>
    /// <param name="character"></param>
    /// <returns></returns>
    [HttpPost]
        public async Task<ActionResult> PostCharacter(CharacterPostDTO character)
        {
            Character c = _mapper.Map<Character>(character);
            await _characterService.AddAsync(c);
            return CreatedAtAction("GetByIdAsync", new {id = c.CharacterId}, c);
        }

        /// <summary>
        /// Delete a character from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            await _characterService.DeleteByIdAsync(id);
            return NoContent();
        }


    }
}
