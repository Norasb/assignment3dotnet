﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTO;
using MovieCharactersAPI.Models.DTO.Characters;
using MovieCharactersAPI.Models.DTO.Movies;
using MovieCharactersAPI.Services.Movies;
using System.Net;

namespace MovieCharactersAPI.Controllers 
{
    [Route("movies")]
    [ApiController]
    public class MovieController : ControllerBase 
    {
        private readonly IMovieService _movieService;
        private readonly IMapper _mapper;

        public MovieController(IMovieService movieService, IMapper mapper) 
        {
            _movieService = movieService;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all the Movies in the database with their related data represented as strings
        /// </summary>
        /// <returns>List of MovieDto</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDto>>> GetAllMovies() 
        {
            return Ok(
                _mapper.Map<List<MovieDto>>(
                await _movieService.GetAllAsync()));
        }

        /// <summary>
        /// Gets one movie with specific id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A single MovieDto</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDto>> GetMovieById(int id) 
        {
            try 
            {
                return Ok(
                    _mapper.Map<MovieDto>(
                    await _movieService.GetByIdAsync(id)));
            } catch (Exception ex) 
            {
                return NotFound(
                    new ProblemDetails() {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Gets all the characters in a specific movie by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list with CharacterDto</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> GetCharactersForMovieAsync(int id) 
        {
            try 
            {
                return Ok(
                    _mapper.Map<List<CharacterDTO>>(
                    await _movieService.GetCharactersInMovieAsync(id)));
            } catch (Exception ex) 
            {
                // Formatting an error code for the exception messages. 
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails() 
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Adds a new movie to the database
        /// </summary>
        /// <param MoviePostDto="movieDto"></param>
        /// <returns>The new movie that is added</returns>
        [HttpPost]
        public async Task<ActionResult> PostMovie(MoviePostDto movieDto) 
        {
            Movie movie = _mapper.Map<Movie>(movieDto);
            await _movieService.AddAsync(movie);
            return CreatedAtAction("GetMovieById", new { id = movie.MovieId }, movie);
        }


        /// <summary>
        /// Changes a movie in the database
        /// </summary>
        /// <param id="id"></param>
        /// <param MoviePutDto="movieDto"></param>
        /// <returns>No content</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovieAsync(int id, MoviePutDto movieDto) 
        {
            Movie putMovie = new Movie() {
                MovieId = id,
                MovieTitle = movieDto.MovieTitle,
                Genre = movieDto.Genre,
                ReleaseYear = movieDto.ReleaseYear,
                Director = movieDto.Director,
                MoviePicture = movieDto.MoviePicture,
                Trailer = movieDto.Trailer
            };

            try {
                await _movieService.UpdateAsync(_mapper.Map<Movie>(putMovie));
                return NoContent();
            } catch (Exception ex) {
                return NotFound(
                    new ProblemDetails() {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                );
            }
        }

            /// <summary>
            /// Adds existing characters to a movie
            /// </summary>
            /// <param name="charactersIds"></param>
            /// <param name="id"></param>
            /// <returns>No content</returns>
            [HttpPut("{id}/characters")]
            public async Task<IActionResult> UpdateCharactersForMovieAsync(int[] charactersIds, int id) 
            {
                try 
                {
                    await _movieService.UpdateCharactersInMovieAsync(charactersIds, id);
                    return NoContent();
                } 
                catch (Exception ex) 
                {
                    return NotFound(
                        new ProblemDetails() 
                        {
                            Detail = ex.Message,
                            Status = ((int)HttpStatusCode.NotFound)
                        }
                        );
                }
            }

            /// <summary>
            /// Deletes a specific movie from the database by id
            /// </summary>
            /// <param name="id"></param>
            /// <returns>No content</returns>
            [HttpDelete("{id}")]
            public async Task<ActionResult> DeleteMovieAsync(int id) 
            {
                try 
                {
                    await _movieService.DeleteByIdAsync(id);
                    return NoContent();
                } 
                catch (Exception ex) 
                {
                    return NotFound(
                        new ProblemDetails() 
                        {
                            Detail = ex.Message,
                            Status = ((int)HttpStatusCode.NotFound)
                        }
                        );
                }
            }
        }
    }
